QUnit.module("SearchManager Tests", function () {
    const searchContainer = document.createElement('div');
    const searchManager = new app.SearchManager({
        searchContainer,
        vimeo : app.vimeo
    });

    const videos =
        [
            {
                video: 'video 1',
                title: 'title 1',
                description: 'description 1'
            },
            {
                video: 'video 2',
                title: 'title 2',
                description: 'description 2'
            },
            {
                video: 'video 3',
                title: 'title 3',
                description: 'description 3'
            },
            {
                video: 'video 4',
                title: 'title 4',
                description: 'description 4'
            },
            {
                video: 'video 5',
                title: 'title 5',
                description: 'description 5'
            },
            {
                video: 'video 6',
                title: 'title 6',
                description: 'description 6'
            },
            {
                video: 'video 7',
                title: 'title 7',
                description: 'description 7'
            },
            {
                video: 'video 8',
                title: 'title 8',
                description: 'description 8'
            },
            {
                video: 'video 9',
                title: 'title 9',
                description: 'description 9'
            },
            {
                video: 'video 10',
                title: 'title 10',
                description: 'description 10'
            },
            {
                video: 'video 11',
                title: 'title 11',
                description: 'description 11'
            },
            {
                video: 'video 12',
                title: 'title 12',
                description: 'description 12'
            },
            {
                video: 'video 13',
                title: 'title 13',
                description: 'description 13'
            },
            {
                video: 'video 14',
                title: 'title 14',
                description: 'description 14'
            },
            {
                video: 'video 15',
                title: 'title 15',
                description: 'description 15'
            },
            {
                video: 'video 16',
                title: 'title 16',
                description: 'description 16'
            },
            {
                video: 'video 17',
                title: 'title 17',
                description: 'description 17'
            },
            {
                video: 'video 18',
                title: 'title 18',
                description: 'description 18'
            },
            {
                video: 'video 19',
                title: 'title 19',
                description: 'description 19'
            },

            {
                video: 'video 20',
                title: 'title 20',
                description: 'description 20'
            }
        ];
    // const UIManagerPrototype = app.app.UIManager;
    
    // const videoContainer = document.createElement('div');
    // const videoTeplate = document.createElement('teplate');
    //     // TODO fil the teplate
    // const paginationContainer = document.createElement('nav');

    // const uiManager = new new app.UIManager({
    //     container: videoContainer,
    //     paginationContainer,
    //     videoTeplate,
    //     vimeo : app.vimeo
    // });

    QUnit.test('test _filterVideos with text', function (assert) {
        var results = searchManager._filterVideos({
            searchText : 'was filmed between 4th and 11th April 2011. I had'
        });
        var expectedResults = [{
            title : 'The Mountain'
        }]

        assert.equal(results[0].title, expectedResults[0].title, "The first's eleent title is ok");

        //TODO other results ( use assert.deepEquel )
    });

    QUnit.test('test _filterVideos with user have more than 10 likes', function (assert) {
        //TODO
        var result = searchManager._filterVideos({
            moreThanTen: true
        });
        var expectedResults = [
        {
            title : 'The Mountain'
        },
        {
            title : 'Landscapes: Volume Two'
        }]
         assert.equal(result[1].title, expectedResults[1].title, "The first's element where more ten is true");                
    });

    QUnit.test('test _filterVideos with text = kls ', function (assert) {
        var results = searchManager._filterVideos({
            searchText : 'kls'
        });
        var expectedResults = []

        assert.equal(results.length,0, "filter with text = kls OK");

    });

    QUnit.test('test search videos with likes < 10 with text search = For licensing footage and more than 10 = false ', function (assert) {
        var result = searchManager._filterVideos({
            searchText : 'For licensing footage',
            moreThanTen: true
        });
        var expectedResults = [
        {
            title : 'Landscapes: Volume Two'
        }]
         assert.equal(result[0].title, expectedResults[0].title, "The first's element where more ten is true"); 
    });


});

// to show we can split tests by modules
QUnit.module("SearchManager Test callBacks", function () {
    const searchContainer = document.createElement('div');
    const searchManager = new app.SearchManager({
        searchContainer,
        vimeo : app.vimeo
    });


    // callback example (async like)
    QUnit.test('testing _callListeners with two callbacks', function(assert){
        var expectedResults = [{
            title : 'The Mountain'
        }];

        var done1 = assert.async(),
            done2 = assert.async();
        searchManager.onChange(({videos, itemsPerPage}) => {
            assert.equal(videos[0].title, expectedResults[0].title, "The 1st callback is called with the right title");
            done1();
        });
        searchManager.onChange(({videos, itemsPerPage}) => {
            assert.equal(videos[0].title, expectedResults[0].title, "The 2nd callback is caled with the right title");
            done2();
        });

        searchManager._callListeners({videos : expectedResults});
    });
});