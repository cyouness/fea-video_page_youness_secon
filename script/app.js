(function () {
    window.app = window.app || {};

    app.UIManager = function ({ container, containerId, videoTemplate, videoTemplateId, paginationContainer, paginationContainerId }) {
        this.videosContainer = container || document.getElementById(containerId);
        this.videoTemplate = videoTemplate || document.getElementById(videoTemplateId);
        this.paginationContainer = paginationContainer || document.getElementById(paginationContainerId);
    };

    app.UIManager.prototype.render = function ({ videos, itemsPerPage }) {
        itemsPerPage = itemsPerPage || 10;

        this._currentPage = 1;
        this._itemsPerPage = itemsPerPage;
        this._videosElements = videos.map((video) => this._creteVideoContainer({ video }));

        this._renderPage();
    };

    app.UIManager.prototype._renderPage = function(){
        var from = (this._currentPage - 1) * this._itemsPerPage,
            to   = Math.min( (this._currentPage) * this._itemsPerPage, this._videosElements.length);
        
        var appendFunc = document.appendChild.bind(this.videosContainer);
        if(this.videosContainer){
        this.videosContainer.innerHTML = "";
        this._videosElements
            .slice(from, to)
            .map((fragment) => document.importNode(fragment, true) )
            .forEach(appendFunc);

        var pagination = this._createPagination();

        this.paginationContainer.innerHTML = '';
        this.paginationContainer.appendChild(pagination);
        }
    }

    app.UIManager.prototype._createPagination = function(){
        var totalItems      = this._videosElements.length,
            currentPage     = this._currentPage,
            itemsPerPage    = this._itemsPerPage;

        //TODO optiize
        var ul = document.createElement('ul');
        ul.classList.add('pager');

        var previous = document.createElement('li'),
            next = document.createElement('li');
        previous.innerHTML = '<a href="#">Previous</a></li>';
        next.innerHTML = '<a href="#">next</a></li>';

        if(totalItems > itemsPerPage){
            var hasNextPage = itemsPerPage * currentPage < totalItems;
            if(currentPage != 1){
                previous.getElementsByTagName('a')[0].addEventListener('click', (e) => {
                    e.preventDefault();
                    this._currentPage --;
                    this._renderPage();
                });
                ul.appendChild(previous);
            }
            if(hasNextPage){
                next.getElementsByTagName('a')[0].addEventListener('click', (e) => {
                    e.preventDefault();
                    this._currentPage ++;
                    this._renderPage();
                });
                ul.appendChild(next);
            }
        }

        return ul;
    }

    app.UIManager.prototype._creteVideoContainer = function ({ video }) {
            if(this.videoTemplate){
        var templateContent = this.videoTemplate.content;

        templateContent.querySelector('.video').innerHTML = video.video;
        templateContent.querySelector('.title').innerHTML = video.title;
        templateContent.querySelector('.description').innerHTML = video.description;

        var clone = document.importNode(templateContent, true);
        
        return clone;
        }
    };

    app.SearchManager = function ({ searchContainer, searchContainerId, vimeo }) {
        this.searchContainer = searchContainer || document.getElementById(searchContainerId);
        this.vimeo = vimeo;
        this._TextSearched = "";
        this._moreThanTen = false;
        this._onChangeListeners = [];
        this._initEventListeners();
    };

    app.SearchManager.prototype._filterVideos = function({searchText, moreThanTen,}){
        searchText = searchText || "";
        moreThanTen = moreThanTen || false;
        return this.vimeo.data
            .map((item) => {
                let userTotalLikes = item.user.metadata.connections.likes.total;
                return {
                    title : item.name,
                    video : item.embed.html,
                    description : item.description,
                    userTotalLikes
                };
            })
            .filter((vid)=>{
                if (vid.description !== null) {

                var idx = vid.description.toLowerCase().indexOf(searchText.toLowerCase());
                var ok = idx !== -1;
                //console.log(vid.description);
                if(moreThanTen){
                    ok = (ok && vid.userTotalLikes >= 10);
                }
                }
                return ok;
            });
    };
    app.SearchManager.prototype._eventsfilter = function (e) {
        let _item = 10;
            if (e.target.checked) {
                this._moreThanTen = true;
            }
            else if(arguments[1] === 'search'){
                this._TextSearched =  e.target.value;
            }
            else if(arguments[1] === 'items'){
                _item = e.target.value;
                console.log(_item);
            }
            else{
                this._moreThanTen = false;
            }
            var videos = this._filterVideos({
                searchText :this._TextSearched,
                moreThanTen: this._moreThanTen,
            });
            

            this._callListeners({videos, itemsPerPage : _item});

    }
    app.SearchManager.prototype._callListeners = function({videos, itemsPerPage}){
        this._onChangeListeners.forEach((calB) => {
            calB.call(null, {videos, itemsPerPage});
        });
    };

    app.SearchManager.prototype.onChange = function(callBack){
        this._onChangeListeners.push(callBack);
    };
    app.SearchManager.prototype._initEventListeners = function(){
        var  searchInput = document.getElementById("quick-search");
        var  checkedInput = document.getElementById("moreTen");
        var  nbItem = document.querySelector("#nbItem");
        if(searchInput){
        searchInput.addEventListener("keyup",(e) => {
            this._eventsfilter(e,'search');
        });
        checkedInput.addEventListener("click",(e) => {
            this._eventsfilter(e);
        });

        nbItem.addEventListener("change",(e) => {
            this._eventsfilter(e,'items');
        });
    }
    };


})();



document.addEventListener("DOMContentLoaded", function () {
    var vimeo = app.vimeo;

    var uiManager = new app.UIManager({
        containerId: 'videos',
        paginationContainerId : 'pagination',
        videoTemplateId : 'video-template',
        vimeo: vimeo
    });

    var searchManager = new app.SearchManager({
        searchContainerId: 'sidebar',
        vimeo : vimeo
    });

    var videos = searchManager._filterVideos({
        searchText: searchManager._TextSearched,
        moreThanTen: searchManager._moreThanTen
    });

    searchManager.onChange(({ videos, itemsPerPage }) => {
        uiManager.render({ videos, itemsPerPage });
    });

    searchManager._callListeners({ videos, itemsPerPage: 10 }); 


    window.d = {
        uiManager,
        searchManager
    };


});